build:
	go build -o bin/git-statuser main.go

run:
	go run main.go -c conf.json
