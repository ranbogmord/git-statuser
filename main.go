package main

import (
	"flag"
	"runtime"
	"os"
	"encoding/json"
	"fmt"
	"sync"
	"os/exec"
	"strings"
	"net/http"
	"bytes"
)

type GitStatuserPath struct {
	ProjectId string `json:"project_id"`
	Path string `json:"path"`
}

type GitStatuserConfig struct {
	GitLabToken string `json:"gitlab_token"`
	GitLabApiUrl string `json:"gitlab_api_url"`
	Paths []GitStatuserPath `json:"paths"`
}

var wg sync.WaitGroup
var conf GitStatuserConfig

func getHomeDir() string {
	if runtime.GOOS == "windows" {
		home := os.Getenv("HOMEDRIVE") + os.Getenv("HOMEPATH")
		if home == "" {
			home = os.Getenv("USERPROFILE")
		}

		return home
	}

	return os.Getenv("HOME")
}

func extractStatus(output string) string {
	if strings.Contains(output, "working tree clean") {
		return "in-sync"
	} else if strings.Contains(output, "not staged for commit") {
		return "uncommited-changes"
	} else if strings.Contains(output, "untracked") {
		return "untracked-files"
	} else {
		return "parse-error"
	}
}

func updateGitlabStatus(dir GitStatuserPath, status string) {
	data := struct {
		Description string `json:"description"`
	}{
		Description: fmt.Sprintf("**Testserver status:** %s", status),
	}

	jsonString, err := json.Marshal(data)
	if err != nil {
		fmt.Println(err)
		return
	}

	req, err := http.NewRequest("PUT", conf.GitLabApiUrl + "/projects/" + dir.ProjectId, bytes.NewReader(jsonString))
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Private-Token", conf.GitLabToken)

	c := http.Client{}
	resp, err := c.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
}

func CheckGitStatus(dir GitStatuserPath) {
	out, err := exec.Command("sh", "-c", fmt.Sprintf("cd %s && git status", dir.Path)).Output()
	if err != nil {
		fmt.Println(err)
	}

	o := strings.ToLower(string(out))
	status := extractStatus(o)

	fmt.Println(dir.Path + ":", status)

	updateGitlabStatus(dir, status)
	wg.Done()
}

func main() {
	confPath := flag.String("c", getHomeDir() + "/.git-statuser-config", "The config path")
	flag.Parse()

	confFile, err := os.Open(*confPath)
	if err != nil {
		panic(err)
	}

	err = json.NewDecoder(confFile).Decode(&conf)
	if err != nil {
		panic(err)
	}

	fmt.Println("Running git status checks")
	wg = sync.WaitGroup{}
	for _, p := range conf.Paths {
		wg.Add(1)
		go CheckGitStatus(p)
	}

	wg.Wait()
}
